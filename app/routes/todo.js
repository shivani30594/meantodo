var express = require('express');
var router = express.Router();

var todoController = require('../controllers/todocontroller.js');

router.get('/api/todos', todoController.listTodos);
router.post('/api/todos', todoController.createTodo);
router.delete('/api/todos/:todo_id', todoController.deleteTodo);

module.exports = router;